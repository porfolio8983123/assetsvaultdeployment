const router = require('express').Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");

router.post("/storeRate", authorization, async (req,res) => {
    try {
        const {rate,asset_id} = req.body;

        

        const newRating = await pool.query("INSERT INTO rating (rate, user_id, asset_id) VALUES ($1, $2, $3) ON CONFLICT (user_id,asset_id) DO UPDATE SET rate = EXCLUDED.rate RETURNING *",[rate,req.user,asset_id]);

        if (newRating.rows.length !== 0) {
            return res.status(200).json({"success":`Thank you for your rating ${rate} ❤️️`})
        } else {
            return res.status(401).json({"error":"Sorry! Something went wrong!"})
        }
    } catch (error) {
        return res.status(500).json({"error":error.message});
    }
})

router.get("/allRating",async (req,res) => {
    try {
        const rating = await pool.query("SELECT * from rating")

        if (rating.rows.length !== 0) {
            return res.status(200).json(rating.rows)
        } else {
            return res.status(401).json("No One Rated!")
        }
    } catch (error) {
        console.error(error.message);
    }
})

module.exports = router;