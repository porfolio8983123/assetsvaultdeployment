const router = require('express').Router();
const pool = require("../db");
const authorization = require("../middleware/authorization");

router.post("/addFeedback",authorization,async (req,res) => {
    try {
        console.log("adding feedback")
        const {message,asset_id} = req.body;
        console.log("user id", req.user);

        const newFeedback = await pool.query("INSERT INTO modelfeedback (message,user_id,asset_id) VALUES ($1,$2,$3) RETURNING *",[
            message,req.user,asset_id
        ]);

        if (newFeedback.rows.length !== 0) {
            res.status(200).json({"success":"Thank you for your time!"})
        } else {
            res.status(401).json({"error":"Sorry! Try again."})
        }
    } catch (error) {
        console.error(error.message);
    }
})

router.get("/getFeedback/:id",async (req,res) => {
    try {
        const {id} = req.params;
        const data = await pool.query("SELECT feed_id,message,created_at,user_name,user_email,encode(profile,'base64') FROM modelfeedback JOIN users ON modelfeedback.user_id = users.user_id WHERE modelfeedback.asset_id = $1",[id]);
        
        if (data.rows.length !== 0) {
            res.status(200).json(data.rows)
        }
    } catch (error) {
        console.error(error.message);
    }
})

router.post("/addReport",authorization, async (req,res) => {
    console.log("adding report")
    try {
        const {asset_id,report} = req.body;

        const newReport = await pool.query("INSERT INTO assetReport (report,user_id,asset_id) VALUES ($1,$2,$3) RETURNING *",[report,req.user,asset_id]);

        if (newReport.rows.length !== 0) {
            return res.status(200).json({success:"Thank You. Let us take action."})
        } else {
            return res.status(401).json({error:"Sorry! Something went wrong."})
        }
    } catch (error) {
        console.error({"error":error.message});
        return res.status(401).json({error:error.message})
    }
})

module.exports = router;