module.exports = function (req,res,next) {
    const {email,name,password,confirm} = req.body;

    function validEmail(userEmail) {
        return /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(userEmail);
    }

    if (req.path === "/register") {
        if (![email,name,password].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        } else if (!validEmail(email)){
            return res.status(401).json("Provide a valid Email address!");
        } else if (password.length < 8) {
            return res.status(401).json("Password must be greater than 8 digit.")
        } else if (password !== confirm) {
            return res.status(401).json("Password does not match!");
        }
    } else if (req.path === "/login") {
        if (![email,password].every(Boolean)) {
            return res.status(401).json("Missing credentials");
        } else if (!validEmail(email)) {
            return res.status(401).json("Invalid Email");
        }
    } else if (req.path === "/sendOTP") {
        if (![email,name,password].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        } else if (!validEmail(email)){
            return res.status(401).json("Provide a valid Email address!");
        } else if (password.length < 8) {
            return res.status(401).json("Password must be greater than 8 digit.")
        } else if (password !== confirm) {
            return res.status(401).json("Password does not match!");
        }
    } else if (req.path === "/validateUser") {
        if (![email,name,password].every(Boolean)) {
            return res.status(401).json("Missing Credentials");
        } else if (!validEmail(email)){
            return res.status(401).json("Provide a valid Email address!");
        } else if (password.length < 8) {
            return res.status(401).json("Password must be greater than 8 digit.")
        } else if (password !== confirm) {
            return res.status(401).json("Password does not match!");
        }
    }

    next();
}