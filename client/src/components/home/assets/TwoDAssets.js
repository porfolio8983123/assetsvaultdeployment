import './threed.css'
import "../../font.css";
import FileLoaderComponent from '../../Model/FileLoaderComponent';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { useLoader } from '@react-three/fiber';
import { Suspense, startTransition, useEffect, useState } from 'react';
import ModelComponent from '../../Model/ModelComponent';
import { Link } from 'react-router-dom';
import './two.css';
import {HiDotsHorizontal, HiOutlineShare} from 'react-icons/hi'

export default function ThreeDAssets() {

    const [models,setModels] = useState([]);
    const [modelt,setModelt] = useState("image");

    async function fetch3DModel() {
        const response = await fetch('http://localhost:5000/auth/getTwoDimension');

        const parseRes = await response.json();

        if (parseRes.data) {
            setModels(parseRes.data);
        } else {
            console.log(parseRes.error)
        }

    }

    useEffect(() => {
        fetch3DModel();
    },[])

    console.log("models ",models);

    return(
        <div className = "container-fluid">
            <div className='video'>

                <div className="video_div"
                    style={{backgroundImage:`url('https://rare-gallery.com/uploads/posts/916512-Eunice-Ye-drawing-Legends-of-Runeterra-rings-weapon.jpg')`, height:500,backgroundRepeat:'no-repeat', backgroundSize:'cover',backgroundPosition:'center'}}
                >
                </div>
                
                <div className = "box d-none d-sm-flex flex-column justify-content-center align-items-center">
                    <div>
                        <h4 className='text-white word fw-bold'>Scroll Down</h4>
                    </div>
                    <div>
                        <span></span>
                        <span></span>
                        <span></span> 
                    </div>
                </div>

                <div className='text-center d-flex justify-content-center align-items-center' style={{
                    position:'absolute',
                    top:"0",
                    left:"55%",
                    height:"100%",
                    width:'40%',
                }}>
                    <div className='p-2'>
                        <p className='h2 text-white' style={{fontWeight:'bold'}}><span style={{color:"#FE4C00"}}>2D</span> ASSETS</p>
                        <p className='text-white' style={{fontSize:10}}>THE WORLD'S PREFERRED SOURCE FOR 3D CONTENT</p>
                    </div>
                </div>

            </div>

            <div  className="header-main border-bottom bg-dark mt-5">
                
            </div>

            <div className="" style={{marginTop:50}}>
                <div className="gallery">
                {models && models.map(data =>
                    // <Link key={data.asset_id} 
                    //     className='text-decoration-none pics'
                    //     to={{
                    //         pathname:`/preview/${data.asset_id}`
                    //     }}
                    // >
                    //     <img src = {`data:image/png;base64,${data.encode}`} alt='My Image'/>
                    // </Link>
                    <Link className='text-decoration-none' to = {{
                        pathname: `/preview/${data.asset_id}`
                    }}>
                        <div className='pics mt-3' key={data.asset_id} style={{backgroundColor:'white',borderRadius:20}}>
                            <img src = {`data:image/png;base64,${data.encode}`} alt='My Image' style={{width:'100%',borderRadius:20}}/>
                        </div>
                    </Link>
                    )}
                </div>
            </div>
        </div>
    )
}