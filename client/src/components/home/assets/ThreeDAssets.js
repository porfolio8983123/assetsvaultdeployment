import './threed.css'
import "../../font.css";
import FileLoaderComponent from '../../Model/FileLoaderComponent';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { useLoader } from '@react-three/fiber';
import { Suspense, startTransition, useEffect, useState } from 'react';
import ModelComponent from '../../Model/ModelComponent';
import { Link } from 'react-router-dom';

export default function ThreeDAssets() {

    const [models,setModels] = useState([]);

    function UseGetFileScene(filePath) {
        const gltf = useLoader(GLTFLoader,`data:model/gltf-binary;base64,${filePath}`);
        return gltf.scene;
    }

    const gltf = useLoader(GLTFLoader,"./assets/uploads/girl1.glb");

    async function fetch3DModel() {
        await fetch('http://localhost:5000/auth/model')
        .then(response => response.json())
        .then(data => setModels(data))
        .catch(error => console.error(error))
    }

    // style={{backgroundImage:`url('https://rare-gallery.com/uploads/posts/916512-Eunice-Ye-drawing-Legends-of-Runeterra-rings-weapon.jpg')`, height:550,backgroundRepeat:'no-repeat', backgroundSize:'cover',backgroundPosition:'center'}}

    useEffect(() => {

        const userId = localStorage.getItem("userId");

        console.log("user id from local storage is ", userId)

        fetch3DModel();
    },[])

    console.log("models ",models);

    return(
        <div className = "container-fluid">
            <div className='video-container'
                style={{
                    position:'relative'
                }}
            >

               <div className='' style={{
                height:500
               }}>
                <Suspense fallback = {<div>Loading...</div>}>
                    <FileLoaderComponent file = {gltf.scene}/>
                </Suspense>
                
               </div>
                
                <div className = "box d-none d-sm-flex flex-column justify-content-center align-items-center">
                    <div>
                        <h4 className='text-white word fw-bold'>Scroll Down</h4>
                    </div>
                    <div>
                        <span></span>
                        <span></span>
                        <span></span> 
                    </div>
                </div>

                <div className='text-center d-flex justify-content-center align-items-center' style={{
                    position:'absolute',
                    top:"0",
                    left:"0",
                    height:"100%",
                    width:'40%',
                }}>
                    <div className='p-2'>
                        <p className='h2 text-white' style={{fontWeight:'bold'}}><span style={{color:"#FE4C00"}}>3D</span> ASSETS</p>
                        <p className='text-white' style={{fontSize:10}}>THE WORLD'S PREFERRED SOURCE FOR 3D CONTENT</p>
                    </div>
                </div>

                <div className='text-center d-flex justify-content-center align-items-center woman' style={{
                    position:'absolute',
                    top:"0",
                    left:"80%",
                    height:"100%",
                    width:'20%',
                    backgroundImage:'url("https://blog.wingfox.com/wp-content/uploads/2022/04/8603-1024x576.png")',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition:'center'
                }}>
                    <div className='p-2'>
                        {/* <p className='h2 text-white' style={{fontWeight:'bold'}}><span style={{color:"#FE4C00"}}>3D</span> ASSETS</p>
                        <p className='heap text-white' style={{fontSize:20, fontWeight:'bold'}}>Royalty-free 3D assets for personal and commercial projects</p> */}
                    </div>
                </div>

                <div className='text-center d-flex justify-content-center align-items-center dark-man' style={{
                    position:'absolute',
                    top:"0",
                    left:"60%",
                    height:"100%",
                    width:'40%',
                    backgroundImage:'url("https://static.dfoneople.com/publish/bbs_data_st1_neople/images/pds/ae5e58115a/timor.png")',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition:'right'
                }}>
                    <div className='p-2'>
                        {/* <p className='h2 text-white' style={{fontWeight:'bold'}}><span style={{color:"#FE4C00"}}>3D</span> ASSETS</p>
                        <p className='heap text-white' style={{fontSize:20, fontWeight:'bold'}}>Royalty-free 3D assets for personal and commercial projects</p> */}
                    </div>
                </div>
            </div>

            <div  className="header-main border-bottom bg-dark mt-5">
                
            </div>

            <div className="container-fluid overflow-hidden" style={{marginTop:30}}>
                <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3">
                {models && models.map(data =>
                    <Link key={data.asset_id} 
                        className='text-decoration-none'
                        to={{
                            pathname:`/preview/${data.asset_id}`
                        }}
                    >
                        <div className="modelContainer col border border-success text-white my-3 mx-3 px-0">
                            <div className='text-white' style={{height:300}}>
                                <ModelComponent file = {UseGetFileScene(data.encode)} type = "small"/>
                            </div>
                            <div className='p-2' style={{backgroundColor:"#FE4C00"}}>
                                <h4 className = "text-white">{data.name.length > 22 ? `${data.name.substring(0,22)}...`:data.name}</h4>
                            </div>
                        </div>
                    </Link>
                    )}
                </div>
            </div>
        </div>
    )
}