import { FaStar } from 'react-icons/fa';
import './rating.css';
import { useEffect, useState } from 'react';
import { useOutletContext } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Rating() {

    const [userId,setUserId] = useState();
    const {id} = useOutletContext();
    const [assetRating,setAssetRating] = useState([]);
    const [five,setFive] = useState(0);
    const [four,setFour] = useState(0);
    const [three,setThree] = useState(0);
    const [two,setTwo] = useState(0);
    const [one,setOne] = useState(0);
    const [singleAsset,setSingleAsset] = useState([])
    const [currentValue,setCurrent] = useState();
    const [totalRating,setTotalRating] = useState();

    const halfWidth = `10%`;
    const borderRadius = '20px';

    const ratingStyle = {
        height: '10px',
        width: '30%',
        borderRadius: borderRadius,
        backgroundColor: '#f1f1f1',
        position: 'relative'
    };

    const halfColorStyle = {
        position: 'absolute',
        top: '0',
        left: '0',
        height: '100%',
        backgroundColor: '#FE4C00',
        borderRadius: `${borderRadius}`
    };

    const handleHover = (position) => {
        if (position === 1) {
            setOne(one + 1)
            setCurrent(position)
        } else if (position === 2) {
            setTwo(two + 1)
            setCurrent(position)
        } else if (position === 3) {
            setThree(three + 1)
            setCurrent(position)
        } else if (position === 4) {
            setFour(four + 1)
            setCurrent(position)
        } else if (position === 5) {
            setFive(five + 1)
            setCurrent(position)
        }
    }

    const handleMouseLeave = (position) => {
        setCurrent(0)
        if (position === 1) {
            setOne(one - 1)
        }  else if (position === 2) {
            setTwo(two - 1)
        } else if (position === 3) {
            setThree(three - 1)
        } else if (position === 4) {
            setFour(four - 1)
        } else if (position === 5) {
            setFive(five - 1)
        }

    }

    async function storeRate() {
        if (currentValue === 1) {
            setOne(one + 1)
        } else if (currentValue === 2) {
            setTwo(two + 1)
        } else if (currentValue === 3) {
            setThree(three + 1)
        } else if (currentValue === 4) {
            setFour(four + 1)
        } else if (currentValue === 5) {
            setFive(five + 1)
        }
        setTotalRating(totalRating + 1)
        try {
            const data = {rate:currentValue,asset_id:id}
            const response = await fetch("http://localhost:5000/rating/storeRate",{
                method:'POST',
                headers:{"Content-Type":"application/json","token":localStorage.token},
                body:JSON.stringify(data)
            });

            const parseRes = await response.json();
            
            if (parseRes.success) {
                toast.success(parseRes.success, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
            } else {
                toast.error(parseRes.error, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
            }

        } catch (error) {
            console.log(error)
        }
    }

    async function getAllRating(){
        const rating = await fetch("http://localhost:5000/rating/allRating");
        const parseRes = await rating.json();
        setAssetRating(parseRes)
    }

    async function organizeRating() {

        let o = 0;
        let t = 0;
        let thre = 0;
        let fou = 0;
        let fiv = 0;

        const thisAsset = assetRating.filter(element => element.asset_id === parseInt(id))
        console.log("single asset",thisAsset)
        setSingleAsset(thisAsset)
        setTotalRating(singleAsset.length)

        singleAsset.forEach(element => {
            console.log("rating ",element.rate);
            if (element.rate === 1) {
                o = o + 1;
            } else if (element.rate === 2) {
                t = t + 1;
            } else if (element.rate === 3) {
                thre = thre + 1;
            } else if (element.rate === 4) {
                fou = fou + 1;
            } else if (element.rate === 5) {
                fiv = fiv + 1;
            }
        });
        setOne(o)
        setTwo(t)
        setThree(thre);
        setFour(fou);
        setFive(fiv);
    }

    useEffect(() => {
        getAllRating();
        return;
    },[id])

    useEffect(() => {
        organizeRating();
        return;
    },[assetRating])

    console.log(currentValue)

    return(
        <div>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
            <div className='d-flex align-items-center'>
                <div className='d-flex justify-content-evenly' style={{width:160}}>
                    <FaStar size={28} className='star-rating' onClick={() => {storeRate()}} onMouseEnter={() => handleHover(1)} onMouseLeave={() => handleMouseLeave(1)}/>
                    <FaStar size={28} className='star-rating' onClick={() => {storeRate()}} onMouseEnter={() => handleHover(2)} onMouseLeave={() => handleMouseLeave(2)}/>
                    <FaStar size={28} className='star-rating' onClick={() => {storeRate()}} onMouseEnter={() => handleHover(3)} onMouseLeave={() => handleMouseLeave(3)}/>
                    <FaStar size={28} className='star-rating' onClick={() => {storeRate()}} onMouseEnter={() => handleHover(4)} onMouseLeave={() => handleMouseLeave(4)}/>
                    <FaStar size={28} className='star-rating' onClick={() => {storeRate()}} onMouseEnter={() => handleHover(5)} onMouseLeave={() => handleMouseLeave(5)}/>
                </div>
                <div className='ms-4 mt-3'>
                    <h6>Rating out of 5</h6>
                </div>
            </div>
            <div className='mt-3'>
                {assetRating && 
                    <h6>This asset has {totalRating} user ratings</h6>
                }
            </div>
            <div className = "mt-3">
                <div className="d-flex align-items-center">
                    <h6>5 star</h6>
                    <div className="bg-light ms-3" style={ratingStyle}>
                        <div style={{...halfColorStyle, width: five?(five/assetRating.length) * 100:0}}></div>
                    </div>
                    <h6 className="ms-3">{five}</h6>
                </div>
                <div className="d-flex align-items-center">
                    <h6>4 star</h6>
                    <div className="bg-light ms-3" style={ratingStyle}>
                        <div style={{...halfColorStyle, width: four?(four/assetRating.length) * 100:0}}></div>
                    </div>
                    <h6 className="ms-3">{four}</h6>
                </div>
                <div className="d-flex align-items-center">
                    <h6>3 star</h6>
                    <div className="bg-light ms-3" style={ratingStyle}>
                        <div style={{...halfColorStyle, width: three?(three/assetRating.length) * 100:0}}></div>
                    </div>
                    <h6 className="ms-3">{three}</h6>
                </div>
                <div className="d-flex align-items-center">
                    <h6>2 star</h6>
                    <div className="bg-light ms-3" style={ratingStyle}>
                        <div style={{...halfColorStyle, width: two?(two/assetRating.length) * 100:0}}></div>
                    </div>
                    <h6 className="ms-3">{two}</h6>
                </div>
                <div className="d-flex align-items-center">
                    <h6>1 star</h6>
                    <div className="bg-light ms-3" style={ratingStyle}>
                        <div style={{...halfColorStyle, width: one?(one/assetRating.length) * 100:0}}></div>
                    </div>
                    <h6 className="ms-3">{one}</h6>
                </div>
            </div>
        </div>
    )
}