import { useEffect, useState } from "react"
import { useOutletContext } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Feedback() {
    const {id} = useOutletContext();
    console.log("model id from feedback ", id)

    const [allData,setAllData] = useState([]);
    const [loading,setLoading] = useState(false);

    async function getAllFeedback() {
        try {
            const response = await fetch(`http://localhost:5000/fdback/getFeedback/${id}`)

            const parseRes = await response.json();
            setAllData(parseRes);
            setLoading(true)
        } catch (error) {
            console.error(error.message);
        }
    }

    useEffect(() => {
        getAllFeedback();
        return;
    },[])

    function getImage(encode) {
        if (encode) {
            const binaryData = atob(encode)
            const byteArray = new Uint8Array(binaryData.length)

            for (let i = 0; i < binaryData.length; i++) {
                byteArray[i] = binaryData.charCodeAt(i);
            }

            const blob = new Blob([byteArray],{type:'image/png'});
            const url = URL.createObjectURL(blob);
            console.log("url",url);
            return url;
        }
    }

    console.log(allData)
    return(
        <div className="container" style={{backgroundColor:'black', padding:20}}>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
            <div className="">
                {allData.length} Comments
            </div>
            { allData && allData.map(data => (
                <div className="d-flex justify-content-between align-items-center mt-5 p-2" style={{borderRadius:10,height:80}} key={data.feed_id}>
                    <div className="d-flex align-items-center">
                        <div className="" style={{width:70,height:70, borderRadius:50}}>
                            <img
                                src = {data.encode?getImage(data.encode):"https://icons-for-free.com/download-icon-person+profile+user+icon-1320184018411209468_512.png"}
                                style={{width:70,height:70, borderRadius:20}}
                            />
                        </div>
                        <div>
                            <h6 className="ms-4 mt-3">{data.user_name}</h6>
                            <div className="ms-4" style={{fontSize:10, marginTop:-8}}>{new Date(data.created_at).toDateString()}</div>
                            <p className="ms-4">{data.message}</p>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}