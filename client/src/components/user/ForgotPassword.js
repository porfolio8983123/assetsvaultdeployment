import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';

export default function ForgotPassword() {

  const navigate = useNavigate();

  const [email,setEmail] = useState();

  function validEmail(userEmail) {
    return /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(userEmail);
  }

  const sendMail = async (event) => {
    event.preventDefault();
    if (!email) {
      console.log("no email")
      return;
    } else if (!validEmail(email)) {
      console.log("Not a valid email")
    } else {
      const data = {email}
      const sendingMail = await fetch("http://localhost:5000/sendPassword",{
        method:'POST',
        headers:{"Content-Type":'application/json'},
        body:JSON.stringify(data)
      })

      const parseRes = await sendingMail.json();

      if (parseRes === 'false') {
        toast.error("Something went wrong. Try Again", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "colored",
          });
      } else {
        setTimeout(() => {
          navigate('/');
        }, 2000);
      }

      console.log(typeof parseRes);
    }
  }

  return (
    <div className='text-white d-flex justify-content-center align-items-center' style={{marginTop:200}}>
      <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
      <div className='text-center'>
        <h4>Forgot Your Password?</h4>
        <h6 className='mb-5'>Please, enter your recovery email address below to reset your password.</h6>

        <form className='mt-3' onSubmit={sendMail}>
            <input className='form-control' placeholder='12200003.gcit@rub.edu.bt' type='email'
              value={email}
              onChange={(event) => setEmail(event.target.value)}
            />
            <button className='btn text-white px-4 mt-3' style={{backgroundColor:"#FE4C00"}}>Get Password</button>
        </form>
      </div>
    </div>
  )
}
