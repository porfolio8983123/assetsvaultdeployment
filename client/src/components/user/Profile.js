import { NavLink, useParams } from "react-router-dom"
import ProfileHeader from "./ProfileHeader";
import AddModel from "../Model/AddModel";
import { FaFileUpload } from "react-icons/fa";
import { useEffect, useState } from "react";
import ModelComponent from "../Model/ModelComponent";
import { useLoader } from "@react-three/fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { Link } from "react-router-dom";
import {AiFillDelete} from 'react-icons/ai';
import '../home/assets/two.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Footer from "../layouts/Footer";
import './profile.css';
import dame from './damedayo.png';

export default function Profile(props) {
    const [userId,setUserId] = useState("");
    const [image,setImage] = useState();
    const [imageDisplayUrl,setImageDisplayUrl] = useState('');
    const [userName,setUserName] = useState("");
    const [email,setEmail] = useState("");
    const [decodedImage,setDecodedImage] = useState("");
    const [modelData,setModelData] = useState([]);
    const [isThreeD,setIsThreeD] = useState(true);
    const [imageData,setImageData] = useState([]);

    const [changeUserName,setChangeUserName] = useState();
    const [changeEmail,setChangeEmail] = useState();
    const [changingPassword,setChangingPassword] = useState(false);

    const [newPassword,setNewPassword] = useState("");
    const [confirmPassword,setConfirmPassword] = useState("");

    const [settingPassword,setSettingPassword] = useState("");

    //deleting
    const [showConfirmation, setShowConfirmation] = useState(null);

    const [refresh,setRefresh] = useState(false);

    const [dataUpdated, setDataUpdated] = useState(false);

    const verifyUpdate = (boolean) => {
        setDataUpdated(boolean);
    }

    const handleFileInputChange = (e) => {
        setImage(e.target.files[0]);

        const reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = () => {
            setImageDisplayUrl(reader.result);
        }
    }

    function UseGetFileScene(filePath) {
        const gltf = useLoader(GLTFLoader,`data:model/gltf-binary;base64,${filePath}`);
        return gltf.scene;
    }

    const uploadChanges = async (event) => {
        event.preventDefault();

        console.log(imageData)

        if (!imageDisplayUrl && userName === changeUserName && changeEmail === email) {
            toast.success("Profile Up to Date", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });
            return;
        }
        
        const reader = new FileReader();
        reader.readAsDataURL(image);
        reader.onload = async () => {
            const imageData = reader.result.split(',')[1];
            const data = {userName,email,image:imageData,userId}

            const response = await fetch("http://localhost:5000/uploadProfile",{
                method:'POST',
                headers:{"Content-Type":"application/json"},
                body:JSON.stringify(data)
            })
            .then(() => {
                toast.success("Successfully Updated!", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
            })
            .catch((error) => {
                console.error(error.message);
            })
        }
    }

    async function getUserData() {
        const response = await fetch(`http://localhost:5000/auth/user`,{
            method:'GET',
            headers:{token: localStorage.token}
        })

        const parseRes = await response.json();
        setEmail(parseRes.user_email);
        setUserName(parseRes.user_name);
        setUserId(parseRes.user_id);

        setChangeEmail(parseRes.user_email)
        setChangeUserName(parseRes.user_name)

        if (parseRes.encode) {
            const binaryData = await atob(parseRes.encode)
            const byteArray = new Uint8Array(binaryData.length)

            for (let i = 0; i < binaryData.length; i++) {
                byteArray[i] = binaryData.charCodeAt(i);
            }

            const blob = new Blob([byteArray],{type:'image/png'});
            const url = URL.createObjectURL(blob);
            setDecodedImage(url);
        }
    }

    const fetchModel = async () => {
        const response = await fetch(`http://localhost:5000/auth/singleModel/${userId}`)

        const parseRes = await response.json();
        setModelData(parseRes);
    }

    const fetchImage = async () => {
        console.log("Fetching image")
        const response = await fetch(`http://localhost:5000/auth/singleModel/twod/${userId}`,{
            method:"GET",
            headers: {"token":localStorage.token}
        });
        const parseRes = await response.json();

        console.log("all image data ", parseRes)

        if (parseRes.success) {
            setImageData(parseRes.success)
        } else {
            console.log("No data")
        }
    }

    const handlePasswordChange = async (event) => {
        event.preventDefault();
        if (!newPassword || !confirmPassword) {
            setSettingPassword("Insert Password");
            return;
        } else if (newPassword !== confirmPassword) {
            setSettingPassword("Password doesn't match");
            return;
        } else if (newPassword.length < 8) {
            setSettingPassword("Password must greater than 8 digits");
            return;
        }
        try {
            const data = {newPassword}
            const resposne = await fetch(`http://localhost:5000/auth/changePassword/${userId}`,{
                method:'POST',
                headers:{"Content-Type":"application/json"},
                body:JSON.stringify(data)
            });

            const parseRes = await resposne.json();
            if (resposne.status === 200) {
                setSettingPassword(parseRes);
            } else {
                setSettingPassword(parseRes);
            }
        } catch (error) {
            console.log(error.message)
        }
    }

    const deleteAssets = async (asset_id) => {
        const response = await fetch(`http://localhost:5000/admin/assets/${asset_id}`,{
            method:'DELETE'
        })
        if (response.ok) {
            setRefresh(!refresh);
        }

        if (!response.ok) {
            toast.error("Deletion Failed!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });
        }
    }

    const handleConfirmation = async (asset_id) => {
        deleteAssets(asset_id)
    }

    useEffect(() => {
        getUserData();
        return;
    },[])

    useEffect(() => {
        fetchImage();
        fetchModel();
        return;
    },[userId,refresh,dataUpdated]);

    console.log('updating ', dataUpdated)
    
    return(
        <div className="container-fluid">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
            <ProfileHeader setAuth = {props.setAuth}/>
            <div className="container d-flex justify-content-center align-items-center mt-4" style={{backgroundColor: "#D9D9D9", borderRadius:20}}>
                <div class="container my-4">
                    <div class="row row-cols-1 row-cols-md-2">
                        <div class="d-flex justify-content-center align-items-center col">
                            <div className="d-flex">
                            <div className="d-flex justify-content-center align-items-center border border-danger" style={{width:200, height:200, borderRadius:100}}>
                                {imageDisplayUrl && 
                                    <img src = {imageDisplayUrl} style={{width:200, height:200, borderRadius:100}}/>
                                }
                                {decodedImage && !imageDisplayUrl && 
                                    <img src = {decodedImage} style={{width:200, height:200, borderRadius:100}}/>
                                }
                                {!imageDisplayUrl && ! decodedImage && 
                                    <img src = "https://api.time.com/wp-content/uploads/2017/06/pikachu-most-influential-game-characters.jpg?quality=85" style={{width:200, height:200, borderRadius:100}}/>

                                }
                            </div>
                            <div className="d-flex justify-content-center align-items-center mt-4 mb-3 ms-3">
                                <label for = "profile">
                                    <FaFileUpload
                                        size={40}
                                        style={{cursor:'pointer'}}
                                    />
                                </label>
                                <input type="file" id = "profile" style={{display:'none'}} accept="image/png, image/jpeg"
                                    onChange={handleFileInputChange}
                                />
                            </div>
                            </div>
                        </div>
                        <div class="col">
                            <form onSubmit={uploadChanges}>
                                <div className="">
                                    <input className="form-control mb-3" type="email" value={email} onChange={(event) => setEmail(event.target.value)}/>
                                    <input className="form-control mb-3" type="text" value={userName} onChange={(event) => setUserName(event.target.value)}/>
                                </div>
                                <div className="d-flex flex-column justify-content-center align-items-center mt-4">
                                    <button className="btn text-white" style={{backgroundColor:"#FE4C00", paddingRight:25,paddingLeft:25}}>Save Changes</button>
                                </div>
                            </form>
                            <div className="d-flex flex-column justify-content-center align-items-center mt-4">
                                {changingPassword ? (
                                    <div
                                        onClick={() => setChangingPassword(false)}
                                        style={{cursor:'pointer', color:'blue'}}
                                    >Close</div>
                                ):(
                                    <div
                                        onClick={() => setChangingPassword(true)}
                                        style={{cursor:'pointer', color:'blue'}}
                                    >Change Password</div>
                                )

                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {changingPassword &&
                <div className="d-flex justify-content-center align-items-center mt-4">
                    <div style={{width:'15%'}}>
                        <form onSubmit={handlePasswordChange}>
                            <input type="password" className="form-control" placeholder="New Password"
                                value={newPassword}
                                onChange={(event) => setNewPassword(event.target.value)}
                            />
                            <input type="password" className="form-control mt-4" placeholder="Confirm Password"
                                value={confirmPassword}
                                onChange={(event) => setConfirmPassword(event.target.value)}
                            />
                            <div className="d-flex flex-column justify-content-center align-items-center mt-4">
                                {settingPassword && 
                                    <p className="fw-bold" style={{color:'red'}}>{settingPassword}</p>
                                }
                                <button className="btn text-white mb-3" style={{backgroundColor:"#FE4C00"}}>Change Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            }

            <div className="container">
                <section className="d-flex justify-content-between align-items-center header-main border-bottom bg-dark mt-4">
                </section>
                
                <div className="mt-4">
                    <button type="button" class="btn text-white" data-bs-toggle="modal" data-bs-target="#staticBackdrop" style={{backgroundColor:"#FE4C00"}}>
                    Add Asset
                    </button>

                    <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">The File Must Be .glb and image Extension</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <AddModel id = {userId} verifyUpdate = {verifyUpdate} dataUpdated = {dataUpdated}/>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                <section className="d-flex justify-content-between align-items-center header-main border-bottom bg-dark mt-4">
                </section>

                <div className="mt-4">
                    <h4 className="text-white border-none">My Assets</h4>
                </div>
                <div className="d-flex">
                    <NavLink className="text-decoration-none" style={{color:isThreeD?"#FE4C00":'white'}} onClick={() =>setIsThreeD(true)}><h5>3 Dimension</h5></NavLink>
                    <NavLink className="text-decoration-none ms-4" style={{color:!isThreeD?"#FE4C00":'white'}} onClick={() =>setIsThreeD(false)}><h5>2 Dimension</h5></NavLink>
                </div>
                <div>
                <div className="container-fluid overflow-hidden" style={{marginTop:20}}>
                <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3">
                {isThreeD && modelData && modelData.length > 0 && modelData.map(data =>
                    <div key={data.asset_id} 
                        style={{position:'relative'}}
                    >
                        <div className="modelContainer col border border-success text-white my-3 mx-3 px-0">
                            <div className='text-white' style={{height:300}}>
                                <ModelComponent file = {UseGetFileScene(data.encode)} type = "small"/>
                            </div>
                            <div className='p-2' style={{backgroundColor:"#FE4C00"}}>
                                <Link
                                    className='text-decoration-none'
                                    to={{
                                        pathname:`/preview/${data.asset_id}`
                                    }}
                                ><h4 className = "text-white">{data.name}</h4></Link>
                            </div>
                        </div>
                        <div className="deleteButton" style={{
                            position:'absolute',
                            top:"8%",
                            left:"76%"
                        }}>
                            {showConfirmation && showConfirmation === data.asset_id ? (
                                <div className="d-flex flex-column justify-content-center align-items-center">
                                    <button onClick={() => handleConfirmation(data.asset_id)} style={{borderRadius:10, marginBottom:5, borderColor:'#FE4C00'}}>Confirm</button>
                                    <button onClick={() => setShowConfirmation(null)} style={{borderRadius:10,borderColor:'#FE4C00',backdropFilter: 'blur(5px)'}}>Cancel</button>
                                </div>
                            ) : (
                                <AiFillDelete
                                    size={35}
                                    color="#FE4C00"
                                    style={{cursor:'pointer'}}
                                    onClick={() => setShowConfirmation(data.asset_id)}
                                />
                            )}
                            
                        </div>
                    </div>
                    )}
                    {isThreeD && modelData.length === 0 && 
                                <div className="d-flex justify-content-center align-items-center border-secondary">
                                    <img className="img-fluid" src={dame}/>
                                </div>

                                }
                <div className="gallery" style={{width:'100%',borderWidth:1, borderColor:'red'}}>
                {!isThreeD && imageData.length !== 0 && imageData.map(data =>
                    <div className='text-decoration-none' to = {{
                        pathname: `/preview/${data.asset_id}`
                    }}>
                        <div className='pics mt-3' key={data.asset_id} style={{backgroundColor:'white',borderRadius:20, position:'relative'}}>
                            <img src = {`data:image/png;base64,${data.encode}`} alt='My Image' style={{width:'100%',borderRadius:20}}/>
                            <div className="deleteButton" style={{
                                position:'absolute',
                                top:"8%",
                                left:"76%"
                            }}>
                                {showConfirmation && showConfirmation === data.asset_id ? (
                                    <div className="d-flex flex-column justify-content-center align-items-center">
                                        <button onClick={() => handleConfirmation(data.asset_id)} style={{borderRadius:10, marginBottom:5, borderColor:'#FE4C00'}}>Confirm</button>
                                        <button onClick={() => setShowConfirmation(null)} style={{borderRadius:10,borderColor:'#FE4C00',backdropFilter: 'blur(5px)'}}>Cancel</button>
                                    </div>
                                ) : (
                                    <AiFillDelete
                                        size={35}
                                        color="#FE4C00"
                                        style={{cursor:'pointer'}}
                                        onClick={() => setShowConfirmation(data.asset_id)}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                    )}
                    {!isThreeD && imageData.length === 0 && 
                    <div className="d-flex justify-content-center align-items-center border-secondary">
                        <img className="img-fluid" src={dame}/>
                    </div>

                    }
                </div>
                </div>
            </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}