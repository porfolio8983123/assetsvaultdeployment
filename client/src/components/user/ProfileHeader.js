import { MdLogout} from 'react-icons/md';
import { Link } from 'react-router-dom';
import '../../components/font.css';

export default function ProfileHeader({setAuth}) {

    async function logout(e) {
        e.preventDefault();
        localStorage.removeItem("token");
        localStorage.removeItem("userId")
        setAuth(false);
    }

    return(
        <div>
            <section className="d-flex justify-content-between align-items-center header-main border-bottom bg-dark">
                <div className="text-white ms-5 mt-3">
                    <Link to="/home" className='text-decoration-none text-white'><h1 style={{fontFamily:'customFont'}}><span style={{color:"#FE4C00"}}>Assets</span>Vault</h1></Link>
                </div>
                <div className="text-white me-5">
                    <MdLogout
                        onClick={logout}
                        size={32}
                        style={{cursor:'pointer'}}
                    />
                </div>
            </section>
        </div>
    )
}