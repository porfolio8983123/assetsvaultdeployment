import React from 'react';
import "./Footer.css"

function Footer() {
  return (
    <div className="text-white text-center text-lg-start" style={{backgroundColor: "#FE4C00", marginTop:30}}>
    <div className="footerbox p-4 border-0">
      <div className="row mt-4">
        <div className="col-lg-3 col-md-12 mb-4 mb-md-0">
          <h5 className="text-uppercase mb-4 fs-4 fw-bold">Assets Vault</h5>

          <p>
          GameDev Market is a marketplace for high quality, affordable game assets handcrafted by talented creators from all around the world.  
          </p>

        </div>
        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">

<hr/>
          
          <ul className="fa-ul" >
            <li className="mb-3" style={{listStyleType: "none"}}>
              <span className="fs-5 fw-bold">Help</span>
            </li>
            <li className="mb-3" style={{listStyleType: "none"}}>
              <span className="lead fs-6"  >assetsvault@zoho.com</span>
            </li>
            <li className="mb-3"style={{listStyleType: "none"}}>
              <span className="lead fs-6">+975 77792293</span>
            </li>
           
          </ul>
        </div>
        <div className="col-lg-3 col-md-6 mb-4 mb-md-0">

<hr/>
          
          <ul className="fa-ul" >
            <li className="mb-3"style={{listStyleType: "none"}}>
              <span className="fs-5 font-weight-bold fw-bold">Feedback</span>
            </li>
            <li className="mb-3"style={{listStyleType: "none"}}>
              <span className="lead fs-6">Leave feedback</span>
            </li>
            
           
          </ul>
        </div>
  
<div className="col-lg-3 col-md-6 mb-4 mb-md-0">
			<hr/>
          
          <ul className="fa-ul" >
            <li className="mb-3"style={{listStyleType: "none"}}>
              <span className="fs-5 font-weight-bold fw-bold">Download</span>
            </li>
            <li className="mb-3" style={{listStyleType: "none"}}>
              <a className='text-decoration-none text-white' href = "https://www.blender.org/"><span className="lead fs-6"  >Blender</span></a>
            </li>
            <li className="mb-3" style={{listStyleType: "none"}}>
              <a className='text-decoration-none text-white' href = "https://www.autodesk.com/products/maya/overview?term=1-YEAR&tab=subscription"><span className="lead fs-6"  >Maya</span></a>
            </li>
           
          </ul>
        </div>
       
      </div>
    </div> 
  </div>
  );
}

export default Footer;
