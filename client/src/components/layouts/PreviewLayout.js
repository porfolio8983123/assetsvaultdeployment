import { NavLink, Outlet } from "react-router-dom";
import { useParams } from "react-router-dom";
import React, { Suspense, useEffect, useState } from "react";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { useLoader } from "@react-three/fiber";
import ModelComponent from "../Model/ModelComponent";
import PreviewHeader from "./PreviewHeader";
import {FaDownload,FaShareSquare} from 'react-icons/fa';
import {saveAs} from 'file-saver';
import LoginModal from "../user/LoginModal";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../home/assets/two.css';
import {AiOutlineSend} from 'react-icons/ai';
import {RxDot} from 'react-icons/rx';
import {FacebookShareButton, FacebookIcon} from 'react-share';
import Footer from "./Footer";

export default function PreviewLayout(props) {
    const {id} = useParams()
    console.log(id);
 
    const [models, setModels] = useState([]);
    const [isOpen,setIsOpen] = useState(false);
    const [report,setReport] = useState();
    const [readyToComment,setReadyToComment] = useState(false);
    const [message, setMessage] = useState();
    const [current,setCurrent] = useState(1)

    const getData = async () => {
        await fetch(`http://localhost:5000/auth/model/${id}`)
        .then(response => response.json())
        .then(data => {
            setModels(data)
            console.log(data);
        })
        .catch(error => console.error(error))
    }

    function UseGetFileScene(filePath) {
        const gltf = useLoader(GLTFLoader,`data:model/gltf-binary;base64,${filePath}`);
        return gltf.scene;
    }

    const downladFile = () => {
        if (models.modeltype === '2D') {
            const blob = new Blob([models.encode], { type: 'image/png' }); // Adjust the MIME type according to your image format
            saveAs(blob, models.name);
        } else {
            const decodedData = atob(models.encode);
            const blob = new Blob([decodedData],{type:'model/gltf-binary'});
            saveAs(blob,models.name);
        }
    }

    const sendReport = async (event) => {
        event.preventDefault();

        const data = {asset_id:id,report:report}
        
        if (!report) {
            toast.error("Please! Can you fill your report.", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
                });
        } else {
            console.log("Adding report in client")
            const response = await fetch("http://localhost:5000/fdback/addReport",{
                method:'POST',
                headers:{"Content-Type":"application/json","token":localStorage.token},
                body:JSON.stringify(data)
            })

            const parseRes = await response.json();

            if (parseRes.success) {
                toast.success(parseRes.success, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                    });
                setReport("");
            } else {
                toast.error(parseRes.error, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
            }
        }
    }

    async function uploadFeedback() {
        console.log("Uploading comment")
        if (!message) {
            toast.error("Give some interesting feedback!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "colored",
            });

            return;
        }
        try { 
            console.log("try done")
            const data = {message:message,asset_id:id}
            const response = await fetch("http://localhost:5000/fdback/addFeedback",{
                method:"POST",
                headers:{token:localStorage.token,"Content-Type":"application/json"},
                body:JSON.stringify(data)
            });

            const parseRes = await response.json();
            console.log(parseRes," from review layout comment")
            
            if (parseRes.success) {
                toast.success(parseRes.success, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
            } else {
                toast.error(parseRes.error, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "colored",
                });
            }

            setMessage("");

        } catch (error) {
            console.error(error.message)
        }
    }

    useEffect(() => {
        getData();
        return;
    },[])

    console.log("from layouts",models.file);
    console.log("report ", report)

    return(
        <div className="">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />

            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
            <header>
                <PreviewHeader id = {id}/>
                <div className="container"> 

                <div class="container text-center text-white">
                    <div class="row row-cols-1 row-cols-lg-2">
                        <div class="col d-flex justify-content-center">
                        {models && models.modeltype === "3D" && (
                            <div className="gallerytwo" style={{borderRadius:20, width:'100%', height:'100%'}}>
                                <ModelComponent className = "pics" file={UseGetFileScene(models.encode)} type = "large" />
                            </div>
                        )}
                        {models && models.modeltype === '2D' && 
                            <div className="gallerytwo d-flex jutify-content-center align-items-center" style={{borderRadius:20}}>
                                <img className="pics" src = {`data:image/png;base64,${models.encode}`} alt='My Image' style={{width:'100%',height:'auto', borderRadius:20}}/>
                            </div>
                        }
                        </div>
                        <div class="col" style={{position:'relative'}}>
                            {models && models.modeltype === '2D' && 
                                <div className="mt-1" style={{paddingBottom:100}}>
                                    <div className="d-flex justify-content-between align-items-center p-3">
                                        <h5>{models.name.length > 20 ? `${models.name.substring(0,20)}...`:models.name}</h5>
                                        <div style={{marginRight:'20%'}}>
                                            <FacebookShareButton
                                                url = {`${models.name}`}
                                                quote={"Dummy text!"}
                                                hashtag="#assetsvault"
                                            >
                                                <FacebookIcon size={32} round/>
                                            </FacebookShareButton>
                                        </div>
                                        

                                    </div>
                                    <div className="ms-3 mt-4" style={{textAlign:'left'}}>
                                        <h5>Compatible Engines</h5>
                                        {models && models.engine && models.engine.map(data =>
                                            <div className="ms-3 mt-3" key = {data}>
                                                    <h6><span><RxDot size={20} color="#FE4C00"/></span>{data}</h6>
                                            </div> 
                                        )}
                                    </div>
                                    <div className="" style={{position:'absolute',top:'85%',width:'95%'}}>
                                        <div className="d-flex align-items-center">
                                            <div style={{width:'70%'}}><input className="form-control" placeholder="Comment..." type="text" style={{width:'100%'}} onFocus={() => setReadyToComment(true)} onBlur={() => setReadyToComment(false)}
                                                value={message}
                                                onChange={(event) => setMessage(event.target.value)}
                                            /></div>
                                            <div className="ms-2">
                                                <AiOutlineSend
                                                    size={30}
                                                    color="#FE4C00"
                                                    style={{cursor:'pointer'}}
                                                    onClick={uploadFeedback}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }

                            {models && models.modeltype === '3D' && 
                                <div className="mt-1" style={{paddingBottom:100}}>
                                    <div className="d-flex justify-content-between align-items-center p-3">
                                        <h5>{models.name}</h5>
                                        <FacebookShareButton
                                            url = {'http://www.assetsvault.com'}
                                            quote={"Dummy text!"}
                                            hashtag="#assetsvault"
                                        >
                                            <FacebookIcon size={32} round/>
                                        </FacebookShareButton>
                                    </div>
                                    <div className="ms-3 mt-4" style={{textAlign:'left'}}>
                                        <h5>Compatible Engines</h5>
                                        {models && models.engine && models.engine.map(data =>
                                            <div className="ms-3 mt-3" key = {data}>
                                                    <h6><span><RxDot size={20} color="#FE4C00"/></span>{data}</h6>
                                            </div> 
                                        )}
                                    </div>
                                    <div className="" style={{position:'absolute',top:'85%',width:'95%'}}>
                                        <div className="d-flex align-items-center">
                                            <div style={{width:'70%'}}><input className="form-control" placeholder="Comment..." type="text" style={{width:'100%'}} onFocus={() => setReadyToComment(true)} onBlur={() => setReadyToComment(false)}
                                                value={message}
                                                onChange={(event) => setMessage(event.target.value)}
                                            /></div>
                                            <div className="ms-2">
                                                <AiOutlineSend
                                                    size={30}
                                                    color="#FE4C00"
                                                    style={{cursor:'pointer'}}
                                                    onClick={uploadFeedback}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
                    <section className="header-main border-bottom bg-dark d-flex justify-content-between align-items-center p-1" style={{marginTop:30}}>
                        <div className="d-flex align-items-center text-white mt-3">
                        <button className="btn" style={{backgroundColor:current === 1 ? "#FE4C00":null}} onClick={() => setCurrent(1)}>
                                <NavLink to = {{
                                    pathname:`/preview/${id}`
                                }} className="text-decoration-none text-white">
                                    <h6>Overview</h6>
                                </NavLink>
                            </button>
                            <button className="btn" style={{backgroundColor:current === 2 ? "#FE4C00":null}} onClick={() => setCurrent(2)}>
                                <NavLink to = {{
                                    pathname:`fileContent`
                                }} className="text-decoration-none text-white">
                                    <h6>Package Content</h6>
                                </NavLink>
                            </button>
                            
                            <button className="btn" style={{backgroundColor:current === 3 ? "#FE4C00":null}} onClick={() => setCurrent(3)}>
                                <NavLink to = {{
                                    pathname:`rating`
                                }} className="text-decoration-none text-white">
                                    <h6>Review</h6>
                                </NavLink>
                            </button>
                            <button className="btn" style={{backgroundColor:current === 4 ? "#FE4C00":null}} onClick={() => setCurrent(4)}>
                                <NavLink to = {{
                                    pathname:`feedback`
                                }} className="text-decoration-none text-white">
                                    <h6>Feedback</h6>
                                </NavLink>
                            </button>
                        </div>
                        <div className="d-flex justify-content-center align-items-center">
                                <FaDownload
                                    size={30}
                                    color="#FE4C00"
                                    style={{cursor:'pointer'}}
                                    onClick={downladFile}
                                />
                            <div className="text-white ms-3">
                                <button onClick={() => setIsOpen(true)}>Report</button>
                                <ToastContainer
                                    position="top-right"
                                    autoClose={5000}
                                    hideProgressBar={false}
                                    newestOnTop={false}
                                    closeOnClick
                                    rtl={false}
                                    pauseOnFocusLoss
                                    draggable
                                    pauseOnHover
                                    theme="colored"
                                />
                                <LoginModal open = {isOpen} onClose={() => setIsOpen(false)}>
                                    <div>
                                        <div className="d-flex justify-content-evenly mb-4">
                                            <h5 style={{fontWeight:'bold'}}>Report Asset?</h5>
                                        </div>
                                        <form onSubmit={sendReport}>
                                            <textarea rows={5} cols={5} style={{width:'100%'}}
                                                value={report}
                                                onChange={(event) => setReport(event.target.value)}
                                            />
                                            <button className="btn mt-3 text-white" style={{width:'100%',backgroundColor:'#FE4C00'}}>Submit</button>
                                        </form>
                                    </div>
                                </LoginModal>
                            </div>
                        </div>

                    </section>
                </div>
            </header>
            <main className="container text-white mt-4">
                <Outlet context = {{id:id}}/>
            </main>
            <Footer/>
        </div>
    )
}

function DropdownItem(){
    return(
      <li className = 'dropdownItem'>
        <p>Hello</p>
      </li>
    );
  }