import { NavLink, Outlet } from "react-router-dom";
import '../../components/home/assets/threed.css';
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import '../../components/font.css';
import Search from "./Search";
import {AiFillFile} from 'react-icons/ai';
import {MdAddToPhotos, MdLogout} from 'react-icons/md';
import AddModel from "../Model/AddModel";
import Footer from "./Footer";
import './home.css';

export default function HomeLayout({setAuth}) {

    const [userName,setUserName] = useState("");
    const [image,setImage] = useState();
    const [isThreeD,setIsThreeD] = useState(true);
    const [isOpen,setIsOpen] = useState(false);
    const [modelData,setModelData] = useState([]);
    const [imageData,setImageData] = useState([]);
    const [searchTerm,setSearchTerm] = useState("");
    const [searchResult,setSearchResult] = useState([]);
    const [show,setShow] = useState(false);
    const [userId,setUserId] = useState();

    const [dataUpdated, setDataUpdated] = useState(false);

    const verifyUpdate = (boolean) => {
        setDataUpdated(boolean);
    }

    async function logout() {
        localStorage.removeItem("token");
        localStorage.removeItem("userId")
        setAuth(false);
    }

    const getUserData = async () => {
        const response = await fetch("http://localhost:5000/auth/user",{
            method:'GET',
            headers:{token:localStorage.token}
        })

        const parseRes = await response.json();
        console.log(parseRes);
        setUserName(parseRes.user_name);
        setImage(parseRes.profile);
        setUserId(parseRes.user_id)

        if (parseRes.encode) {
            const binaryData = await atob(parseRes.encode)
            const byteArray = new Uint8Array(binaryData.length)

            for (let i = 0; i < binaryData.length; i++) {
                byteArray[i] = binaryData.charCodeAt(i);
            }

            const blob = new Blob([byteArray],{type:'image/png'});
            const url = URL.createObjectURL(blob);
            console.log("url",url);
            setImage(url);
        }
    }

    const getModelData = async () => {
        try {
            const response = await fetch("http://localhost:5000/auth/searchModel");
            const parseRes = await response.json();
            console.log("The response is ", parseRes);
            if (parseRes) {
                setModelData(parseRes);
            }
        } catch (error) {
            console.log(error.message)
        }
    }

    const getImageData = async () => {
        try {
            const response = await fetch("http://localhost:5000/auth/searchImage");
            const parseRes = await response.json();
            if (parseRes) {
                setImageData(parseRes);
            }
        } catch (error) {
            console.log(error.message)
        }
    }

    const handleSearch = (event) => {
        if (isThreeD) {
            const value = event.target.value.toLowerCase();
            setSearchTerm(value)
            const filtered = modelData.filter(item=> 
                    item.name.toLowerCase().includes(value)
                )
            setSearchResult(filtered);
        } else {
            const value = event.target.value.toLowerCase();
            setSearchTerm(value)
            const filtered = imageData.filter(item=> 
                    item.name.toLowerCase().includes(value)
                )
            setSearchResult(filtered);
        }
    }

    useEffect(()=> {
        getUserData();
        getModelData();
        getImageData();
        return;
    },[])

    return(
        <div className = "">
            <header style={{position:"-webkit-sticky", position:'sticky',top:0,zIndex:10}}>
                <section className="header-main border-bottom bg-dark">
                    <div className="container-fluid">
                    <div className="row p-2 pt-3 pb-3 d-flex align-items-center">
                        <div className="col-md-2 logo">
                            <h1 className="d-md-flex text-white" style={{fontFamily:'customFont'}}><span style={{color:"#FE4C00"}}>Assets</span>Vault</h1>
                        </div>
                        <div className="col-md-8">
                        <div className="d-flex justify-content-between align-items-center form-inputs">
                            <input className="form-control rounded-3" type="text" placeholder="Search any assets..." onFocus={() => setIsOpen(true)}
                                style={{width:'90%'}}
                            />
                            <div className="ms-2">
                            <MdAddToPhotos
                                size={32}
                                color="white"
                                type="button"
                                data-bs-toggle = "modal"
                                data-bs-target = "#staticBackdro"
                            />
                            </div>

                            <div className="ms-4">
                            <MdLogout
                                size={32}
                                color="white"
                                onClick={logout}
                                style={{cursor:'pointer'}}
                            />
                            </div>

                            <div className="smallProfile d-flex d-none d-md-flex flex-row align-items-center">
                                <div className="d-flex flex-column ms-2">
                                    <div className="d-flex justify-content-center align-items-center ms-2">
                                        <Link to = {{
                                            pathname: `/profile`
                                        }}>
                                            <div className="ms-3" style={{width:50,height:50, borderRadius:50}}>
                                                {!image && 
                                                    <img src = "https://api.time.com/wp-content/uploads/2017/06/pikachu-most-influential-game-characters.jpg?quality=85" style={{width:50,height:50, borderRadius:50}}/>
                                                }
                                                { image && 

                                                    <img src = {image} style={{width:50,height:50, borderRadius:50}}/>
                                                
                                                }
                                                
                                            </div>
                                        </Link>
                                    </div>    
                                </div>    
                            </div>  

                            <Search open = {isOpen} onClose={() => setIsOpen(false)}>
                                <div>
                                    <div className="input-group rounded">
                                        <input type="search" className="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" 
                                            onChange={handleSearch}
                                            style={{width:'100%'}}
                                            autoFocus
                                        />
                                        <span className="input-group-text border-0" id="search-addon">
                                        </span>
                                    </div>
                                    <div className="overflow-auto" style={{height:300}}>
                                        {searchResult.length > 0 &&
                                            searchResult.map(data => (
                                               <Link className="text-decoration-none"
                                               to={{
                                                    pathname:`/preview/${data.asset_id}`
                                                }}
                                                key={data.asset_id}
                                               >
                                                 <div className="d-flex align-items-center border border-dark mt-3 rounded" style={{}}>
                                                    <div className="d-flex align-items-center p-3">
                                                        <AiFillFile size={25}/>
                                                        <h6 className="ms-2">{data.name}</h6>
                                                    </div>
                                                </div>
                                               </Link>
                                            ))
                                        }
                                    </div>

                                    <div>
                                        <h6>@assetsvault</h6>
                                    </div>
                                </div>
                            </Search>
                        </div>
                        </div>
                        
                        <div className="col-md-2"
                            style={{position:'relative'}}
                        >
                            <div className="d-flex d-none d-md-flex flex-row align-items-center ms-4">
                                <div className="d-flex flex-column ms-2">
                                    <div className="d-flex justify-content-center align-items-center ms-2">
                                        <div>
                                            <span className="qty text-white">{userName}</span>
                                        </div>
                                        <Link to = {{
                                            pathname: `/profile`
                                        }}>
                                            <div className="ms-3" style={{width:50,height:50, borderRadius:50}}>
                                                {!image && 
                                                    <img src = "https://api.time.com/wp-content/uploads/2017/06/pikachu-most-influential-game-characters.jpg?quality=85" style={{width:50,height:50, borderRadius:50}}/>
                                                }
                                                { image && 

                                                    <img src = {image} style={{width:50,height:50, borderRadius:50}}/>
                                                
                                                }
                                                
                                            </div>
                                        </Link>
                                    </div>    
                                </div>    
                            </div>         

                        </div>
                    </div>
                    </div> 
                </section>

                <nav className="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
                <div className="container-fluid">
                    <a className="navbar-brand d-md-none d-md-flex text-white" href="#">Categories</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink to = "/home" className = "nav-link text-decoration-none" aria-current = "page" style={{
                                color:isThreeD?"#FE4C00":"white"
                            }}
                            onClick={() => {
                                setIsThreeD(true)
                                setSearchResult([])
                            }}
                            ><b>3 Dimensions</b></NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to = "2d" className="nav-link text-decoration-none"
                                style={{
                                    color:!isThreeD?"#FE4C00":"white"
                                }}
                                onClick={() => {
                                    setIsThreeD(false)
                                    setSearchResult([]);
                                }}
                            ><b>2 Dimensions</b></NavLink>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>
            </header>
            <main>

                <Outlet />
            </main>

            <Footer/>

            <div className="modal fade" id="staticBackdro" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">The File Must Be .glb and image Extension</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <AddModel id = {userId} verifyUpdate = {verifyUpdate} dataUpdated = {dataUpdated}/>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    )
}