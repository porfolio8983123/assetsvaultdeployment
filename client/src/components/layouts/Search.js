import React from 'react';
import ReactDOM from 'react-dom';
import {FaRegTimesCircle} from 'react-icons/fa';

const MODAL_STYLES = {
    position:'fixed',
    top:'50%',
    left:'50%',
    transform:'translate(-50%,-50%)',
    backgroundColor:'#FFF',
    padding:'50px',
    zIndex:1000,
    maxWidth: 400,
    width: '100%'
}

const OVERLAY_STYLES = {
    position:'fixed',
    top:0,
    left:0,
    right:0,
    bottom:0,
    backgroundColor:'rgba(0,0,0,.7)',
    zIndex:1000
}

const button_styles = {
    position:'absolute',
    top:10,
    left:'90%',
    color:'black',
    cursor:'pointer'
}

function Search({open,children,onClose}) {
    if (!open) return null;
  return ReactDOM.createPortal(
    <>
        <div style={OVERLAY_STYLES}/>
        <div style={MODAL_STYLES} className=''>
            <FaRegTimesCircle size={32} onClick={onClose} style={button_styles}/>
            {children}
        </div>
    </>,
    document.getElementById('portal')
  )
}

export default Search;
