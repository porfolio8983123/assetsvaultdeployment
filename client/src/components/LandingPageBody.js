import React from "react";
import photo1 from "../images/photo1.png";
import "./font.css";
import "./LandingPageBody.css";

function LandingPageBody() {
    return(
        <div className=""> 
            <div>
            <div className="row justify-content-md-around mt-5 align-items-center ms-5 me-5">
                <div className="col-xl-6">
                <div className="Description">
                    <h1 className="text-white" style={{fontFamily:'customFont'}}>Start Your Game Development Here!</h1>
                    <p className="text-white">Using a game assets repository can save developers a significant amount of time and effort in the game development process, allowing them to focus on other aspects of game creation such as gameplay mechanics and level design. It can also help game developers who may not have the resources to create their own assets from scratch to create high-quality games.</p>
                    <div className="d-flex flex-column flex-md-row justify-content-around align-items-center pt-5">
                        <button className="button mt-5 mt-md-0" style={{fontFamily:'customFont'}}><a className="text-white text-decoration-none" href = "https://sketchfab.com/search?q=free+3d+model+sci+fi&type=models" target="_blank">Discover More</a></button>
                    </div>
                </div>
                </div>
                <div className="col-xl-6">
                    <div className="d-flex justify-content-center">
                        <img className="img-fluid" src = {photo1} alt = "character"/>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}

export default LandingPageBody;