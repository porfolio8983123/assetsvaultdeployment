import React, { useState, useEffect } from 'react';

function Dashboard() {
  const [totalUsers, setTotalUsers] = useState(0);
  const [totalAssets, setTotalAssets] = useState(0);

    useEffect(() => {
        async function fetchTotalUsers() {
            const response = await fetch("http://localhost:5000/admin/totalUsers", {
                headers: {"token":'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiN2ZlZDEyOGUtNTFlNy00N2EzLWEzYzctNDY1Zjk4MTRiNTIwIiwiaWF0IjoxNjg2NjIxNTA0LCJleHAiOjE2ODY2MjUxMDR9.QaoW9DIxUYLyPha_skzMjJJQS7aVsTJ2Jfe1hEulqWY'}
            });
            const data = await response.json();
            setTotalUsers(data.totalUsers);
        }
        async function fetchTotalAssets() {
            const response = await fetch("http://localhost:5000/admin/totalAssets", {
                headers: {"token":'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiN2ZlZDEyOGUtNTFlNy00N2EzLWEzYzctNDY1Zjk4MTRiNTIwIiwiaWF0IjoxNjg2NjIxNTA0LCJleHAiOjE2ODY2MjUxMDR9.QaoW9DIxUYLyPha_skzMjJJQS7aVsTJ2Jfe1hEulqWY'}
            });
            const data = await response.json();
            setTotalAssets(data.totalAssets);
        }
        fetchTotalUsers();
        fetchTotalAssets();
    }, []);

  return (
    <div className=' container'>
        <div className=' container'>
            <h2 className=' mt-5 text-light'><span style={{color:"#FE4C00"}}>Admin</span> Dashboard</h2>
        </div>
        <hr className='mt-5 text-light'/>
        <div className='container'>
            <div className="row">
                <div className="col-sm-6 p-5 text-center">
                    <div className='border form-control rounded p-5 text-center'>
                        <p className='h3' style={{color:"#FE4C00"}}>Total User</p><p className='h4'>{totalUsers}</p>
                    </div>
                </div>
                <div className="col-sm-6 p-5 text-center">
                    <div className='border form-control rounded p-5 text-center'>
                        <p className='h3' style={{color:"#FE4C00"}}>Total Assets</p><p className='h4'>{totalAssets}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
}

export default Dashboard;