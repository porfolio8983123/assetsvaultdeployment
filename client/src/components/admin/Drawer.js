import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import './Drawer.css';
import { Outlet } from 'react-router-dom';

// Import your Dashboard, Users, and Assets components
import Dashboard from './Dashboard';
import Users from './Users';
import Assets from './Assets';

function Drawer() {
  const location = useLocation();
  const [showLinks, setShowLinks] = useState(true);
  const [toggleEnabled, setToggleEnabled] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      const shouldShowLinks = window.innerWidth > 992;
      setShowLinks(shouldShowLinks);
      setToggleEnabled(!shouldShowLinks);
    };

    window.addEventListener('resize', handleResize);
    handleResize(); // Check on initial load
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const handleToggle = () => {
    setShowLinks(!showLinks);
  };

  return (
    <div className="container-fluid p-5">
      <div className="row">
        <div className={`col-md-${showLinks ? '3' : '12'}`}>
          <div className="sidenav">
            <div className="sidenav-content border border-light p-3">
              <div className='text-start'>
                <h4 className='text-light pt-3'>
                  <span style={{ color: "#FE4C00" }}>Assets</span> Vault
                  {toggleEnabled && (
                    <button
                      className={`btn btn-light text-dark float-end ${showLinks ? 'active' : ''}`}
                      onClick={handleToggle}
                    >
                      {showLinks ? 'Hide' : 'Show'}
                    </button>
                  )}
                </h4>
              </div>
              {showLinks && (
                <>
                  <hr className='text-light' />
                  <Link to="/dashboard" className={`sidenav-item text-light ${location.pathname === '/dashboard' ? 'active' : ''}`}>Dashboard</Link>
                  <Link to="/users" className={`sidenav-item text-light ${location.pathname === '/users' ? 'active' : ''}`}>View Users</Link>
                  <Link to="/assets" className={`sidenav-item text-light ${location.pathname === '/assets' ? 'active' : ''}`}>View Assets</Link>
                  <Link to="/logout" className="sidenav-item text-light ">Logout</Link>
                </>
              )}
            </div>
          </div>
        </div>

        <div className={`col-md-${showLinks ? '9' : '12'}`}>
          <div className="content border border-light p-3">
  
            <Outlet/>

          </div>
        </div>
      </div>
    </div>
  );
}

export default Drawer;
